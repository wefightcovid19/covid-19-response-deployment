#!/bin/bash

DOCKER_SERVER=https://153876780401.dkr.ecr.us-east-2.amazonaws.com
DOCKER_USERNAME=AWS
DOCKER_PASSWORD="$(aws --profile covid-19-response ecr get-login-password --region us-east-2)"

kubectl create namespace development
kubectl config set-context --current --namespace=development
kubectl delete secret aws-ecr || true
kubectl create secret docker-registry aws-ecr \
  --docker-server=$DOCKER_SERVER \
  --docker-username=$DOCKER_USERNAME \
  --docker-password=$DOCKER_PASSWORD