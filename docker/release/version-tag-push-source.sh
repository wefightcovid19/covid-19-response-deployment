#!/bin/bash

if [ "$#" -ne 1 ]; then
  echo "Usage: $0 VERSION" >&2
  exit 1
fi

cd ../../../covid-19-response-proxy/docker/release
./version-tag-push-source.sh $1
cd ../../../covid-19-response-auth/docker/release
./version-tag-push-source.sh $1
cd ../../../covid-19-response-temperature-tracker/docker/release
./version-tag-push-source.sh $1
