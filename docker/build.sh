#!/bin/bash

cd ../../covid-19-response-proxy/docker/
./build.sh
cd ../../covid-19-response-auth/docker/
./build.sh
cd ../../covid-19-response-temperature-tracker/docker/
./build.sh
cd ../../covid-19-response-database/docker/
./build.sh
