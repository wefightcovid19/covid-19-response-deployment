#!/bin/bash

if [ "$#" -ne 2 ]; then
  echo "Usage: $0 SOURCE_TAG TARGET_TAG" >&2
  exit 1
fi

cd ../../covid-19-response-proxy/docker/
./tag.sh $1 $2
cd ../../covid-19-response-auth/docker/
./tag.sh $1 $2
cd ../../covid-19-response-temperature-tracker/docker/
./tag.sh $1 $2
