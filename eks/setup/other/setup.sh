export AWS_PROFILE=covid-19-response
eksctl create cluster -f cluster.yaml

kubectl config set-context --current --namespace=test
kubectl apply -f external-dns-test.yaml
