#!/bin/bash

if [ "$#" -ne 1 ]; then
  echo "Usage: $0 VERSION" >&2
  exit 1
fi

cd ../../../covid-19-response-proxy/docker/release
./pull-tag-push-image.sh $1
cd ../../../covid-19-response-auth/docker/release
./pull-tag-push-image.sh $1
cd ../../../covid-19-response-temperature-tracker/docker/release
./pull-tag-push-image.sh $1
