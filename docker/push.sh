#!/bin/bash

if [ "$#" -ne 1 ]; then
  echo "Usage: $0 TAG" >&2
  exit 1
fi

cd ../../covid-19-response-proxy/docker/
./push.sh $1
cd ../../covid-19-response-auth/docker/
./push.sh $1
cd ../../covid-19-response-temperature-tracker/docker/
./push.sh $1
